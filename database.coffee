mongoose = require 'mongoose'

mongoUrl = 'mongodb://127.0.0.1:27017/scraper'

console.log 'Connecting to MongoDB server...'

mongoose.connect mongoUrl, {}, ->
  console.log 'Connected to MongoDB server.'

snapshotSchema = new mongoose.Schema
  time: Date
  players: [
    _id: false
    game: String
    count: Number
    url: String
  ]

gameSchema = new mongoose.Schema
  name: String
  url: String
  players: [
    _id: false
    time: Date
    count: Number
  ]

module.exports =
  Game: mongoose.model 'Game', gameSchema
  Snapshot: mongoose.model 'Snapshot', snapshotSchema
