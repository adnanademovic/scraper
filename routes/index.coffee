express = require 'express'
assert = require 'assert'
database = require '../database'
router = express.Router()

# GET home page.
router.get '/', (req, res, next) ->
  if req.query.game?
    await database.Game.findOne
      name: req.query.game
    , defer err, entry
    unless entry
      res.redirect '/'
      return

    assert.equal err, null
    res.render 'index-game',
      category: 'home'
      title: 'Game stats'
      name: entry.name
      url: entry.url
      players: entry.players
  else
    await database.Game.find {},
      _id: false
      name: true
    ,
      sort:
        name: 1
    , defer err, entries

    assert.equal err, null
    res.render 'index',
      category: 'home'
      title: 'Steam scraper'
      games: entries

module.exports = router
