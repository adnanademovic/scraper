express = require 'express'
cron = require 'cron'
jsdom = require 'node-jsdom'
assert = require 'assert'
database = require '../database'
router = express.Router()

scraperRunning = false
scraper = new cron.CronJob
  cronTime: '0 */5 * * * *'
  onTick: ->
    cronTick()
  start: scraperRunning

cronTick = ->
  parseNumber = (number) ->
    parseInt number.split(',').join ''
  await jsdom.env
    url: "http://store.steampowered.com/stats/"
    scripts: ["http://code.jquery.com/jquery.js"]
    features:
      FetchExternalResources: ['script']
      ProcessExternalResources: ['script']
      MutationEvents: '2.0'
    done: defer err, window
  $ = window.$
  time = new Date()
  players = for item in $(".player_count_row")
    game: $(item).find("a").text()
    count: parseNumber $(item).find("span.currentServers").first().text()
    url: $(item).find("a").attr("href")
  dataEntry = new database.Snapshot
    time: time
    players: players
  dataEntry.save()

  for game in players
    conditions =
      url: game.url
    doc =
      $setOnInsert:
        name: game.game
        url: game.url
      $push:
        players:
          time: time
          count: game.count
    options =
      upsert: true
    database.Game.update conditions, doc, options, (err, raw) ->
      assert.equal err, null

  console.log "Fetched update from Steam store stats."

# GET home page.
router.get '/', (req, res, next) ->
  res.render 'cronjob',
    category: 'cronjob'
    title: 'Scraper Admin'
    scraperRunning: scraperRunning
    baseUrl: req.baseUrl

# POST start cron.
router.post '/start', (req, res, next) ->
  unless scraperRunning
    scraperRunning = true
    scraper.start()
  res.redirect '.'

# POST start cron.
router.post '/stop', (req, res, next) ->
  if scraperRunning
    scraperRunning = false
    scraper.stop()
  res.redirect '.'

module.exports = router
